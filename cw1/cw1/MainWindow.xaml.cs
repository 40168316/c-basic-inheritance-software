﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Globalization;

namespace cw1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window 
    {
        // Declaration of variables
        // Variables are for taking data of the form before they are passed into a Class
        private int credits;
        private int uniyear;
        private int matricno;
        private int levelint;
        private int creditsint;
        private string finalaward;
        private DateTime date;
        // R1active tells me if ResearchStudent is active
        bool r1active = false;
        // Stores the students current course before it is overwritten by PhD if researchstudent
        string currentcourse;
        // RShasbeensetonce tell me if ResearchStudent has been set ever
        bool RShasbeensetonce = false;
        // Rsfields tell me if the Research fields/text boxes are active or not
        bool RSfields = false;

        // Declaration of object s1 for class student
        Student s1 = new Student();

        // Create a new object called r1 however make it equal to null as it is only active on the lower if statement
        ResearchStudent r1 = null;

        public MainWindow()
        {  
            InitializeComponent();
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        // When the Advance button is pressed
        private void Advance_btn_Click(object sender, RoutedEventArgs e)
        {
            // Call the Levelvalidate method and make that equal to uniyear
            uniyear = Levelvalidate();
            // Call the Creditsvalidate method and make that equal to credits
            credits = Creditsvalidate();

            // Call the advance method, passing in uniyear and credits and make the return equal the variable uniyear
            uniyear = s1.advance(uniyear, credits);

            // The (altered) value is then returned to the uniyear variable and converted to a string so it can be printed into the textbox
            string leveladvance = uniyear.ToString();
            Level_txtbox.Text = leveladvance;         
        }

        // When the Award button is pressed
        private void Award_btn_Click(object sender, RoutedEventArgs e)
        {
            // Call the Levelvalidate method and make that equal to uniyear
            uniyear = Levelvalidate();
            // Call the Creditsvalidate method and make that equal to credits
            credits = Creditsvalidate();

            // Call the award method, passing in uniyear and credits
            s1.award(credits, uniyear);

            // If s1.awards is equal to 5 then
            if (s1.Award == 5)
            {
                // Display error message
                MessageBox.Show("Student is a ResearchStudent however is not level 4 so cannont get an award.");
            }
            // Else allowing all the other values to have an award form
            else 
            {
                // If the value in Award in Student class is equal to 1 then
                if (s1.Award == 1)
                {
                    // The award will be
                    finalaward = "Certificate of Higher Education";
                }
                // If the value in Award in Student class is equal to 2 then
                else if (s1.Award == 2)
                {
                    // The award will be
                    finalaward = "Degree";
                }
                // If the value in Award in Student class is equal to 3 then
                else if (s1.Award == 3)
                {
                    // The award will be 
                    finalaward = "Honours Degree";
                }
                // If the value in Award in Student class is equal to 4 then
                else if (s1.Award == 4)
                {
                    // The award will be
                    finalaward = "Doctor of Philosophy";
                }

                // Declaration of object form Awardform for form Award
                Award Awardform = new Award();

                // Making the text boxes in Awardform equal to the values taken in by the get set accessors in student class
                // Note that some of these need to be converted to string to be displayed
                Awardform.first_name_txtbox.Text = s1.FirstName;
                Awardform.second_name_txtbox.Text = s1.SecondName;
                Awardform.course_txtbox.Text = s1.Course;
                string matricnostring = Convert.ToString(s1.Matriculation_Number);
                Awardform.matric_no_txtbox.Text = matricnostring;
                string levelstring = Convert.ToString(s1.Level);
                Awardform.level_txtbox.Text = levelstring;
                Awardform.award_txtbox.Text = finalaward;

                // Shows the Awardform
                Awardform.Show();
            }
        }

        // When the set button is pressed
        private void Set_btn__Click(object sender, RoutedEventArgs e)
        {
            // Try block for validating First Name 
            try
            {
                // Takes the input from the textbox (firstname_txtbox) and passes it into FirstName
                s1.FirstName = firstname_txtbox.Text;
            }
            // If the value in the firstname text box cannot be passed into Firstname then it will be caught and it will call the error message
            catch (Exception excep)
            {
                // Display error message
                MessageBox.Show(excep.Message);
            }

            // Try block for validating Second Name
            try
            {
                // Takes the input from the textbox (secondname_txtbox) and passes it into SecondName
                s1.SecondName = secondname_txtbox.Text;
            }
            // If the value in the secondname text box cannot be passed into Secondname then it will be caught and it will call the error message
            catch (Exception excep)
            {
                // Display error message
                MessageBox.Show(excep.Message);
            }

            // Try block for validating Course
            try
            {
                // Takes the input from the textbox (Course_txtbox) and passes it into Course
                s1.Course = Course_txtbox.Text;
            }
            // If the value in the course text box cannot be passed into Course then it will be caught and it will call the error message
            catch (Exception excep)
            {
                // Display error message
                MessageBox.Show(excep.Message);
            }

            // This below sets the date up to be a British date (dd/mm/yyyy) and with help of the try block on a british date can be enter
            // This mean that no invalid date can eb enter either
            CultureInfo ci = new CultureInfo("en-GB");
            Thread.CurrentThread.CurrentCulture = ci;
            
            // Try block for validating Date of Birth
            try
            {
                // If the input in the text box is not empty or null then
                if (!string.IsNullOrEmpty(Date_of_Birth_txtbox.Text))
                {
                    // Try convert the input in the text box to DateTime
                    if (DateTime.TryParse(Date_of_Birth_txtbox.Text, out date)== false)
                    {
                        // If the input in is not a valid DateTime then set the value to minvalue 
                        date = DateTime.MinValue;
                        // Pass date into Date_of_Birth
                        s1.Date_of_Birth = date;
                    }
                    else
                    {
                        // Pass date into Date_of_Birth
                        s1.Date_of_Birth = date;
                    }
                }
                // If the input in the textbox is empty or null then
                else
                {
                    // If the input in is not a valid DateTime then set the value to minvalue 
                    date = DateTime.MinValue;
                    // Pass date into Date_of_Birth
                    s1.Date_of_Birth = date;
                }
            }
            // If converting the value in the text box to int fails or the string is empty or null then it will be caught and it will call the error message
            catch (Exception excep)
            {
                // Display error message
                MessageBox.Show(excep.Message);
            }

            // Try block for validating Matriculation Number
            try
            {
                // If the input in the text box is not empty or null then
                if (!string.IsNullOrEmpty(Matric_number_txtbox.Text))
                {
                    // Try convert the input in the text box to an int
                    if (int.TryParse(Matric_number_txtbox.Text, out matricno) == false)
                    {
                        // If the input in is not a number then set the value to minus one which is invalid
                        matricno = -1;
                        // Pass creditsint into Credits
                        s1.Matriculation_Number = matricno;
                    }
                    else
                    {
                        // If the input is a number then pass it straight into Matriculation_Number
                        s1.Matriculation_Number = matricno;
                    }
                }
                // If the input in the textbox is empty or null then
                else
                {
                    // Make credits equal to minus one as it is an invalid value and pass it in
                    matricno = -1;
                    s1.Matriculation_Number = matricno;
                }
            }
            // If converting the value in the text box to int fails or the string is empty or null then it will be caught and it will call the error message
            catch (Exception excep)
            {
                // Display error message
                MessageBox.Show(excep.Message);
            }

            // Call the Levelvalidate method
            Levelvalidate();

            // Call the Creditsvalidate method
            Creditsvalidate();

            // If topic_txtbox is not empty or supervisor_txtbox is not empty then
            if ((!string.IsNullOrWhiteSpace(topic_txtbox.Text)) || (!string.IsNullOrWhiteSpace(supervisor_txtbox.Text)))
            {
                // Make r1 equal to new ResearchStudent
                r1 = new ResearchStudent();
                // Make s1.Course equal currentcourse before the value is overwritten
                currentcourse = s1.Course; 
                // Set S1.Course to equal PhD
                s1.Course = "PhD";
                // Make Course_txtbox equal to "PhD"
                Course_txtbox.Text = "PhD";
                // Set RShasbeensetonce equal to true
                RShasbeensetonce = true;
                // Set r1active equal to true 
                r1active = true;
                // Pass true into R1Active is student class 
                s1.R1Active = true;
                // Set Rsfields equal to true
                RSfields = true;
            }
            else
            {
                // Set RSfields equal to flase
                RSfields = false;
                // If ResearchStudent has been set once is true then
                if (RShasbeensetonce == true)
                {
                    // Change currentcourse back to s1.Course
                    s1.Course = currentcourse;
                    // Put currentcourse in course textbox
                    Course_txtbox.Text = currentcourse;
                }
            }

                // if r1active is true then
                if (r1active == true)
                {
                // Try block for validating Topic 
                try
                {
                    // Takes the input from the textbox (topic_txtbox) and passes it into Topic in ResearchStudent
                    r1.Topic = topic_txtbox.Text;
                }
                // If the value in the Topic text box cannot be passed into Topic or does not meet validation inside the class then it will be caught and it will call the error message
                catch (Exception excep)
                {
                    // Display error message
                    MessageBox.Show(excep.Message);
                }

                // Try block for validating 
                try
                {
                    // Takes the input from the textbox (supervisor_txtbox) and passes it into Supervisor in ResearchStudent
                    r1.Supervisor = supervisor_txtbox.Text;
                }
                // If the value in the supervisor text box cannot be passed into Supervisor or does not meet validation inside the class then it will be caught and it will call the error message
                catch (Exception excep)
                {
                    // Display error message
                    MessageBox.Show(excep.Message);
                }
            }
            // Reset r1active to false after as user could of entered data into one of these boxes (topic or supervisor) and not meant it, but if they go to press set in the future they would get an error            
            r1active = false;
        }

        // When the clear button is pressed
        private void Clear_btn__Click(object sender, RoutedEventArgs e)
        {
            // Removing the contents of the text boxes by emtpying all the strings in the text boxes
            firstname_txtbox.Text = String.Empty;
            secondname_txtbox.Text = String.Empty;
            Date_of_Birth_txtbox.Text = String.Empty;
            Course_txtbox.Text = String.Empty;
            Matric_number_txtbox.Text = String.Empty;
            Level_txtbox.Text = String.Empty;
            Credits_txtbox.Text = String.Empty;
            topic_txtbox.Text = String.Empty;
            supervisor_txtbox.Text = String.Empty;
        }

        // When the Get button is pressed
        private void Get_btn_Click(object sender, RoutedEventArgs e)
        {
            // Getting all the valus of the student class and updating the text boxes 
            // Note that some of these need to be converted to string to output
            firstname_txtbox.Text = s1.FirstName;
            secondname_txtbox.Text = s1.SecondName;
            // Date is also converted here to short to remove time
            string datestring = s1.Date_of_Birth.ToShortDateString();
            Date_of_Birth_txtbox.Text = datestring;
            Course_txtbox.Text = s1.Course;
            string matricnostring = Convert.ToString(s1.Matriculation_Number);
            Matric_number_txtbox.Text = matricnostring;
            string levelstring = Convert.ToString(s1.Level);
            Level_txtbox.Text = levelstring;
            string creditsstring = Convert.ToString(s1.Credits);
            Credits_txtbox.Text = creditsstring;

            // If RSfields is equal to true then
            if (RSfields == true)
            {
                // Display topic and supervisor
                topic_txtbox.Text = r1.Topic;
                supervisor_txtbox.Text = r1.Supervisor;
            }
        }

        // Method called Creditsvalidate which does validation which cannot be done inside the class. This is checking for null or empty due
        // to the fact that int.TryParse will default to 0 if a text box stay empty. In some cases this is a problem as 0 is allowed in the
        // credits range(0 - 480) but there is a possibility the text box could be empty but it would still be valid when it should not be.
        // Creditsvalidate method was created because there are three situations when you have to validate the value in the credit text box, 
        // once when setting the value, another for the award method and the final one for the advance method. Making a method is more effiicent. 
        public int Creditsvalidate()
        {    
            // Try block for Credits
            try
            {
                // If the input in the text box is not empty or null then
                if (!string.IsNullOrEmpty(Credits_txtbox.Text))
                {
                    // Try convert the input in the text box to an int
                     if (int.TryParse(Credits_txtbox.Text, out creditsint) == false)
                    {
                        // If the input in is not a number then set the value to minus one which is invalid
                        creditsint = -1;
                        // Pass creditsint into Credits
                        s1.Credits = creditsint;
                    }
                    else
                    {
                        // If the input is a number then pass it straight into Credits
                        s1.Credits = creditsint;
                    }
                }
                // If the input in the textbox is empty or null then
                else
                {
                    // Make credits equal to minus one as it is an invalid value and pass it in
                    creditsint = -1;
                    s1.Credits = creditsint;
                }
            }
            // If converting the value in the text box to int fails or the string is empty or null then it will be caught and it will call the error message
            catch (Exception excep)
            {
                // Display error message
                MessageBox.Show(excep.Message);
            }
            // Returning creditsint
            return creditsint;
        }

        // Method Levelvalidate called  which does validation which cannot be done inside the class. This is the exact same as the Creditsvalidate however it 
        // validates the level, which cannot be null, or empty and must be an integer.
        public int Levelvalidate()
        {    
            // Try block for Credits
            try
            {
                // If the input in the text box is not empty or null then
                if (!string.IsNullOrEmpty(Level_txtbox.Text))
                {
                    // Try convert the input in the text box to an int
                    if (int.TryParse(Level_txtbox.Text, out levelint) == false)
                    {
                        // If the input in is not a number then set the value to minus one which is invalid
                        levelint = -1;
                        // Pass creditsint into Credits
                        s1.Level = levelint;
                    }
                    else
                    {
                        // If the input is a number then pass it straight into Credits
                        s1.Level =levelint;
                    }
                }
                // If the input in the textbox is empty or null then
                else
                {
                    // Make credits equal to minus one as it is an invalid value and pass it in
                    levelint = -1;
                    s1.Level = levelint;
                }
            }
            // If converting the value in the text box to int fails or the string is empty or null then it will be caught and it will call the error message
            catch (Exception excep)
            {
                // Display error message
                MessageBox.Show(excep.Message);
            }
            // Return levelint
            return levelint;
        }
    }
}