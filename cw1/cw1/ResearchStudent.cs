﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1
{
    // Calum Templeton 
    // Declaring of a Class called ReseachStudent which inhertits Student Class. This class stores details of ResearchStudents
    // Date of last modification - 22/10/2015
    class ResearchStudent : Student
    {
        // Declaring private fields
        private string topic;
        private string supervisor;

        public string Topic // property for manipulating topic
        {
            // Get accessor
            get
            {
                // Returning the value of private topic
                return topic;
            }
            // Set accessor
            set
            {
                // Validating by is value null or empty
                if (string.IsNullOrEmpty(value))
                {
                    // Throws a new arguement exception with an error message
                    throw new ArgumentException("Topic box is empty. Please enter something!");
                }
                // Will make value equal to topic if value is valid
                topic = value;
            }
        }

        public string Supervisor // property for manipulating supervisor
        {
            // Get accessor
            get
            {
                // Returning the value of private supervisor
                return supervisor;
            }
            // Set accessor
            set
            {
                // Validating by is value null or empty
                if (string.IsNullOrEmpty(value))
                {
                    // Throws a new arguement exception with an error message
                    throw new ArgumentException("Supervisor box is empty. Please enter something!");
                }
                // Will make value equal to supervisor if value is valid
                supervisor = value;
            }
        }
    }
}
