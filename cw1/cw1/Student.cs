﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Globalization;


namespace cw1
{
    // Calum Templeton 
    // Declaring of a Class called Student which stores details of students
    // Date of last modification - 22/10/2015
    public class Student
    {
        // Declaring private fields 
        private string firstName;
        private string secondName;
        private DateTime date_of_Birth;
        private int matriculation_Number;
        private string course;
        private int level;
        private int credits;
        private int getaward;
        private bool r1active;
       
        // Advance method which takes in two values passed in from the form called year and credits
        public int advance(int year, int credits)
        {
            // If the year is equal to 4 then student can't advance so display error message 
            if (year == 4)
            {
                // Error message
                System.Windows.MessageBox.Show("Student is already in 4th year and therefore cannot advance!");

            }
            // If year from level textbox is equal to 3
            else if (year == 3)
            {
                // If credits from credits textbox is equal to or great than 360
                if (credits >= 360)
                {
                    // change year value from 3 to 4
                    year = 4;
                    // Display a message saying student has moved on a year
                    System.Windows.MessageBox.Show("Student has been moved into 4th year!");
                }
                // If credits are not equal to or greater than 360 then
                else
                {
                    // Display error message
                    System.Windows.MessageBox.Show("Student cannot move into 4th year as they do not have enough credits!");
                }
            }
            // If year from level textbox is equal to 2 
            else if (year == 2)
            {
                // If credits from credits textbox is equal to or great than 240
                if (credits >= 240)
                {
                    // change year value from 2 to 3
                    year = 3;
                    // Display a message saying student has moved on a year
                    System.Windows.MessageBox.Show("Student has been moved into 3rd year!");
                }
                // If credits are not equal to or greater than 240 then
                else
                {
                    // Display error message
                    System.Windows.MessageBox.Show("Student cannot move into 3rd year as they do not have enough credits!");
                }
            }
            // If year from level textbox is equal to 1 
            else if (year == 1)
            {
                // If credits from credits textbox is equal to or great than 120 
                if (credits >= 120)
                {
                    // change year value from 1 to 2
                    year = 2;
                    // Display a message saying student has moved on a year
                    System.Windows.MessageBox.Show("Student has been moved into 2nd year!");
                }
                // If credits are not equal to or greater than 120 then
                else
                {
                    // Display error message
                    System.Windows.MessageBox.Show("Student cannot move into 2nd year as they do not have enough credits!");
                }
            }

            // Return the (altered) year value
            return year;
        }

        // Award method which takes in one values passed in from the form called credits. This is a void due to no return
        public int award(int credits, int level)
        {
            // If credits is greater than or equal to 0 and less than or equal to 359 then
            if ((credits >= 0) && (credits <= 359))
            {
                // If r1 (ResearchStudent) is active/true then
                if (r1active == true)
                {
                    // Make getaward equal to 5
                    getaward = 5;
                }
                else
                {
                    // Set getaward to 1
                    getaward = 1;
                }
            }
            // If credits is greater than or equal to 360 and less than or equal to 479 then
            else if ((credits >= 360) && (credits <= 479))
            {
                // If r1 (ResearchStudent) is active/true then
                if (r1active == true)
                {
                    // Make getaward equal to 5
                    getaward = 5;
                }
                else
                {
                    // Set getaward to 2
                    getaward = 2;
                }     
            }
            // If credits is greater than or equal to 480 then
            else if (credits >= 480)
            {
                // If r1active is true and level is equal to 4 then 
                if ((r1active == true) && (level == 4))
                {
                    // Set getaward to 4
                    getaward = 4;
                }
                else
                {
                    // Set getaward to 3
                    getaward = 3;
                }
            }
            // Make r1active equal to false for reset
            r1active = false;
            // Return the altered getaward value
            return getaward;
        }

        public int Matriculation_Number // property for manipulating martric_no
        {
            // Get accessor
            get
            {
                // Returning the value of private matriculation_Number
                return matriculation_Number;
            }
            // Set accessor
            set
            {
                // Validating by if value is less than or equal to 39999 or more than or equal to 60001 then 
                if ((value <= 39999) || (value >= 60001))
                {
                    // Throws a new arguement exception with an error message
                    throw new ArgumentException("Matriculation number invalid! Please enter a value between 40000 and 60000");
                }
                // Will make value equal to matriculation_Number if value is valid
                matriculation_Number = value;
            }
        }

        public string FirstName // property for manipulating firstname
        {
            // Get accessor
            get
            {
                // Returning the value of private firstname
                return firstName;
            }
            // Set accessor
            set
            {
                // Validating by is value null or empty
                if (string.IsNullOrEmpty(value))
                {
                    // Throws a new arguement exception with an error message
                    throw new ArgumentException("First name box is empty. Please enter something!");
                }
                // Will make value equal to firstname if value is valid
                firstName = value;
            }
        }

        public string SecondName // property for manipulating secondname
        {
            // Get accessor
            get
            {
                // Returning the value of private secondname
                return secondName;
            }
            // Set accessor
            set
            {
                // Validating by is value null or empty
                if (string.IsNullOrEmpty(value))
                {
                    // Throws a new arguement exception with an error message
                    throw new ArgumentException("Second name box is empty. Please enter something!");
                }
                // Will make value equal to secondname if value is valid
                secondName = value;
            }
        }

        public DateTime Date_of_Birth // property for manipulating date_of_Birth
        {
            // Get accessor
            get
            {
                // Returning the value of private date_of_birth
                return date_of_Birth;
            }
            // Set accessor
            set
            {
                // If value is equal to nothing then
                 if (DateTime.Compare(value, DateTime.MinValue) == 0)
                {
                    // Throws a new arguement exception with an error message
                    throw new ArgumentException("Date is invalid! Please enter in the form dd/mm/yyyy");
                }
                // Will make value equal to date_of_birth if value is valid
                date_of_Birth = value;
            }  
        }

        public string Course // property for manipulating Course
        {
            // Get accessor
            get
            {
                // Returning the value of private course
                return course;
            }
            // Set accessor
            set
            {
                // Validating by is value null or empty
                if (string.IsNullOrEmpty(value))
                {
                    // Throws a new arguement exception with an error message
                    throw new ArgumentException("Course box is empty. Please enter something!");
                }
                // Will make value equal to course if value is valid
                course = value;
            }
        }

        public int Level // property for manipulating level
        {
            // Get accessor
            get
            {
                // Returning the value of private level
                return level;
            }
            // Set accessor
            set
            {
                // Validating by if the value is less than or equal to 0 or greater than or equal to 5 then 
                if ((value <= 0) || (value >= 5))
                {
                    // Throws a new arguement exception with an error message
                    throw new ArgumentException("Invalid Level integer. Please enter a integer between 1 and 4!");
                }
                // Will make value equal to level if value is valid
                level = value;
            }
        }

        public int Credits // property for manipulating credits
        {
            // Get accessor
            get
            {
                // Returning the value of private credits
                return credits;
            }
            // Set accessor
            set
            {
                // Validating by if the value is less than 0 or greater 480 then 
                if ((value < 0) || (value > 480))
                {
                    // Throws a new arguement exception with an error message
                    throw new ArgumentException("Invalid Credit amount! Please enter a integer between 0 and 480!");
                }
                // Will make value equal to credits if value is valid
                credits = value;
            }
        }

        public int Award // property for manipulating getaward
        {
            // Get accessor
            get
            {
                // Returning the value of private getaward
                return getaward;
            }
            // Set accessor
            set
            {
                // Setting getaward to the value passed in
                getaward = value;
            }
        }

        public bool R1Active // property for R1Active which is working out if Research Student is active or not
        {
            // Get accessor
            get
            {
                return r1active;
            }
            //Set accessor
            set
            {
                r1active = value;
            }
        }
    }
}


