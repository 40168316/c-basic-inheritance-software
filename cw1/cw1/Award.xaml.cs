﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cw1
{
    /// <summary>
    /// Interaction logic for Award.xaml
    /// </summary>
    public partial class Award : Window
    {
        public Award()
        {
            InitializeComponent();
        }

        // When the close button is pressed
        private void close_btn_Click(object sender, RoutedEventArgs e)
        {
            // Hides the current form which is open which is Award
            Hide();
        }
    }
}
